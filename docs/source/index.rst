.. Grupo 2 - DISW documentation master file, created by
   sphinx-quickstart on Fri Sep  3 09:25:49 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Grupo 2 - DISW's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   mark2.md
   modules.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
