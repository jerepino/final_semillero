Introducción
============

Este trabajo práctico integrador forma parte del curso de nivelación dictado por el Laboratorio de Sistemas Embebidos (LSE) de la Facultad de Ingeniería de la Universidad de Buenos Aires (FIUBA) por pedido de INVAP. El curso se lo denomina "Capacitación Semillero DISW". El curso está compuesto por seis módulos: Herramientas, programación en C, programación en C++, programación en Python, lenguaje UML y desarrollo de Sistemas Embebidos. Este trabajo da un cierre al curso.

El trabajo consta de desarrollar un sistema de adquisición, recolección y procesamiento de datos.

A continuación se puede ver una imagen de la aplicación a realizar.

.. uml::

    !include ../../uml/paquetes.plantuml

Motivation
**********

As a newbie experimenter in the field of machine learning, it's easy to forget about the mathematical and statistical
foundations behind the principal models, especially due to the vast amount of high-level packages available with a
plug-and-play philosophy. This package is implemented purely in NumPy and completely vectorized to explain the model's
functionalities for educational purposes.

Limitations
***********

Right now, only a couple of models are implemented, such as :py:class:`business.SingletonDataAcquirer.SingletonDataAcquirer` for unsupervised learning
and regression models like :py:class:`business.SingletonDataAcquirer.SingletonMeta` for supervised learning. Under no
circumstances should this package be used for production environments, only for educational purposes.
