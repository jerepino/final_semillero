presenter module
================

ProcessorViewer
---------------

.. automodule:: presenter.ProcessorViewer
   :members:
   :undoc-members:
   :show-inheritance:
