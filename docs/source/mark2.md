# Introduccion
Este trabajo práctico integrador forma parte del curso de nivelación dictado por el Laboratorio de Sistemas Embebidos (LSE) de la Facultad de Ingeniería de la Universidad de Buenos Aires (FIUBA) por pedido de INVAP. El curso se lo denomina "Capacitación Semillero DISW". El curso está compuesto por seis módulos: Herramientas, programación en C, programación en C++, programación en Python, lenguaje UML y desarrollo de Sistemas Embebidos. Este trabajo da un cierre al curso.

El trabajo consta de desarrollar un sistema de adquisición, recolección y procesamiento de datos.
A continuación se puede ver una imagen de la aplicación a realizar.

![consigna_trabajo](../../images/consigna_trabajo.png)

## Herramientas utilizadas

### Protocolo MQTT
MQTT es el protocolo de comunicación enfocado a la conectividad Machine-to-Machine (M2M), hace referencia a la tecnología que permite a los indicadores o dispositivos la comunicación entre ellos de forma inalámbrica.
![mqtt-architecture](../../images/mqtt-architecture.png)

### IOT
La internet de las cosas ​ es un concepto que se refiere a una interconexión digital de objetos cotidianos con internet

### Placa desarrollo ESP32
Está basado en el procesador de 32 bits Xtensa LX6 dual core hasta 240MHz, incluye soporte WIFI encriptado, Bluetooth 4.2, amplificador de audio, cargador de baterías, interfaz para pulsadores touch, etc, etc.
Se dice que es el primer modulo ESP32 es el primer chip realmente IoT

### Sistema operativo de tiempo real
Un sistema operativo de tiempo real es aquel que ha sido desarrollado para aplicaciones de tiempo real. Como tal, se le exige corrección en sus respuestas bajo ciertas restricciones de tiempo. Si no las respeta, se dirá que el sistema ha fallado.


## Módulo adquisición / Sistema embebido

1. Inicializa el snpt con wifi
2. Luego ejecuta la tarea timestamp_task
3. timestamp_task setea los parametros necesarios,toma la zona horaria y da inicio al mqtt con el
   cliente
4. luego dentro del while se llama a sense() ,que es el ADC1 tomando datos y los almacena en temp
   almacena el resto de varaibles y rellena un buffer con los datos obtenidos
   (id,temp,timestrmp)
5. se llama esp_mqtt_client_publish() quien se encarga de publicar en el cliente con el TOPIC
   correspondiente

[Programa adquisición](https://gitlab.com/nshanahan/esp32-mqtt)

![embebido_secuencia](../../images/embebido_secuencia.png)

### Módulo recolección, procesamiento y presentación
#### Casos de uso
![casos_uso](../../images/casos_uso.png)

Se utilizó un modelo en capas, con la siguiente configuracion
![arquitectura](../../images/arquitectura.jpeg)

Se utilizaron los siguientes patrones de diseño

#### Patron singleton

Su intención consiste en garantizar que una clase solo tenga una instancia y proporcionar un punto de acceso global a ella.

[Patrón Singleton](https://refactoring.guru/es/design-patterns/singleton)

![1024px-Singleton_UML_class_diagram.svg](../../images/1024px-Singleton_UML_class_diagram.svg.png)


#### Patron composite

El patrón Composite sirve para construir objetos complejos a partir de otros más simples y similares entre sí, gracias a la composición recursiva y a una estructura en forma de árbol.

[Patrón Composite](https://refactoring.guru/es/design-patterns/composite)

![CompositeClassDiagram](../../images/CompositeClassDiagram.png)

#### Diagrama de clases

![paquetes](../../images/clases.png)




### Ejemplos de presentación de datos

#### Estadisticas
![maxs](../../images/maxs.png)

![means](../../images/means.png)

![mins](../../images/mins.png)


#### Evolución temperatura vs tiempo
![node_Jeremias](../../images/node_Jeremias.png)

![node_Jesus](../../images/node_Jesus.png)

![node_Martin](../../images/node_Martin.png)

![node_Nicolas](../../images/node_Nicolas.png)



## Autores
Grupo 2:
* Nicolas Shanahan (FSW)
* Jesus Prieto (Testing FSW)
* Jeremías Pino (DSS)
* Martín Castellano (SCT)
