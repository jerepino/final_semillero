business module
===============

SingletonDataAcquirer
---------------------

.. automodule:: business.SingletonDataAcquirer
   :members:
   :undoc-members:
   :show-inheritance:
