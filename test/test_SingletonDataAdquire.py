import pytest
from src.business.SingletonDataAcquirer import SingletonDataAcquirer


@pytest.fixture()
def singleton_data_acquirer():
    data_acquirer = SingletonDataAcquirer('181.47.10.131', 1883, "sensores/nodo_10")

    return data_acquirer


def test_check_data(data_acquirer):

    assert data_acquirer.port == 1883
    assert data_acquirer.broker == '181.47.10.131'
    assert data_acquirer.topic == "sensores/nodo_10"

def test_connect_to_broker(data_acquirer):
    data_acquirer.connect_to_broker()
