
import os
import re
from os.path import abspath
from os.path import dirname
from os.path import join
from setuptools import setup

CURDIR = dirname(abspath(__file__))
PACKAGE_NAME = "src/sensor"

with open("README.md", "r") as fh:
    long_description = fh.read()

with open(join(CURDIR, PACKAGE_NAME, "version.py")) as f:
    match = re.search('VERSION = "(.*)"', f.read())
    if match is None:
        VERSION = "1.0.0"
    else:
        VERSION = match.group(1)
with open(join(CURDIR, "requirements.txt"), encoding="utf-8") as f:
    REQUIREMENTS = f.read().splitlines()

with open(join(CURDIR, "dev_requirements.txt"), encoding="utf-8") as f:
    DEV_REQUIREMENTS = f.read().splitlines()

with open(join(CURDIR, "LICENSE"), encoding="utf-8") as f:
    LICENSE = f.read()

setup(
    name='src.sensor',
    version=VERSION,
    platforms=["linux_x86_64"],
    install_requires=REQUIREMENTS,
    extras_require={
        "dev": [DEV_REQUIREMENTS], 
    },
    long_description=long_description,
    long_description_content_type="text/markdown",
    description='mide temp y publica!',
    py_modules=["src.sensor"],
    package_dir={'src/sensor': ''},
    packages=[
        ""
    ],
    author="grupo N° 2",
    author_email="mcastellano@invap.com.ar",
    license=LICENSE,
    classifiers=[
        "Development Status :: - Production",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.7",
        "Operating System :: linux_x86_64",
        "Topic :: SENSOR :: TEMP",
    ],



)
