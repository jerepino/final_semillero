import random
import json
from paho.mqtt import client as mqtt_client
from persistence.Csv import Csv

"""Clase Singleton"""
class SingletonMeta(type):

    _instances = {}

    def __call__(cls, *args, **kwargs):
        """

        Los posibles cambios en el valor del argumento `__init__` no afectan
        la instancia retornada.

        """
        if cls not in cls._instances:
            instance = super().__call__(*args, **kwargs)
            cls._instances[cls] = instance
        return cls._instances[cls]

class SingletonDataAcquirer(metaclass=SingletonMeta):
    """Clase para el manejo de la adquisicion de los datos provenientes del broker"""

    def __init__(self, broker, port, topic, client_id = f'python-mqtt-{random.randint(0, 100)}', username = 'emqx', password = 'public'):
        self.broker = broker
        self.port = port
        self.topic = topic
        self.client_id = client_id
        self.username = username
        self.password = password
        self.message = ""

    def print_data(self):
        """Imprime resumen de informacion: ip, puerto, topico"""

        print(self.broker)
        print(self.port)
        print(self.topic)
        print(self.client_id)
        print(self.username)
        print(self.password)

    def connect_to_broker(self) -> mqtt_client:
        """Instancia y retorna el cliente mqtt a ser utilizado"""

        def on_connect(client, userdata, flags, rc):
            if rc == 0:
                print("Connected to MQTT Broker!")
            else:
                print("Failed to connect, return code %d\n", rc)

        client = mqtt_client.Client(self.client_id)
        client.on_connect = on_connect
        client.connect(self.broker, self.port)
        return client

    def callback_function(self, client: mqtt_client, data):
        """Metodo ejecutado cada vez que se recibe un mensaje
        
        Al recibir un mensaje, lo analiza y descompone para luego agregarlo a al
        archivo separado por comas
        """

        def on_message(client, userdata, msg):
            print(f"Received `{msg.payload.decode()}` from `{msg.topic}` topic")
            try:
                x = msg.payload.decode("utf-8")
                # parse x:
                y = json.loads(x)
                # the result is a Python dictionary:
                id = int(y["Id"])
                mytemp = float(y["temp"])
                time_stamp = str(y["timestamp"])
                data.append(id, mytemp,time_stamp)

            except:  # includes simplejson.decoder.JSONDecodeError
                print('Decoding JSON has failed')

        client.subscribe(self.topic)
        client.on_message = on_message


if __name__ == '__main__':
    pass

