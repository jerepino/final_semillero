from persistence.Csv import Csv
from presenter.ProcessorViewer import *


if __name__ == "__main__":

    data = Csv()
    composite = Plotter()

    leaf = min()
    composite.add(leaf)
    
    leaf = max()
    composite.add(leaf)

    leaf = mean()
    composite.add(leaf)

    leaf = temp_vs_time()
    composite.add(leaf)

    composite.display(data.get())