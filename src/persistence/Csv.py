from .Repository import Repository
import pandas as pd


class Csv(Repository):
    """Clase utilizada para el manejo de archivo separado por comas"""

    def __init__(self):
        self.columns = ['Id', 'temp', 'timestamp']
        self.path = '../data/'
        self.name = 'data.csv'


    def create(self, path=None, name=None):
        """Crea un archivo tipo csv con el nombre y path pasados por argumento
        
        Si no se proveen argumentos el archivo,
        se usan por defecto --> DEFAULT_PATH = '../../data/' y DEFAULT_NAME = 'data.csv'
        """
        
        if path is not None:
            self.path = path
        if name is not None:
            self.name = name

        self.df = pd.DataFrame(columns = self.columns)
        self.df.to_csv(self.path + self.name)


    def append(self, id, data, time_stamp):
        """Agrega un mensaje al archivo csv creado desde metodo create"""

        self.df = self.df.append( {   'Id': id,
                                    'temp': data,
                                    'timestamp': time_stamp},
                                    ignore_index = True   )

        self.df.to_csv(self.path + self.name)



    def get(self):
        """Retorna el dataframe completo con el contenido dentro del archivo csv"""

        self.df = pd.read_csv(self.path + self.name)
        return self.df
