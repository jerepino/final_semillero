from presenter.ProcessorViewer import *
from persistence.Csv import Csv
from business.SingletonDataAcquirer import SingletonDataAcquirer

IP = '181.47.10.131'
PORT =  1883
TOPIC = "sensores/nodo_10"

if __name__ == "__main__":
    fileCSV = Csv()
    fileCSV.create()

    data_acquirer = SingletonDataAcquirer(IP, PORT, TOPIC)
    data_acquirer.print_data()

    client = data_acquirer.connect_to_broker()
    data_acquirer.callback_function(client, fileCSV)
    client.loop_forever()
