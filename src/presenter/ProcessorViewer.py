import abc
import matplotlib.pyplot as plt


class ProcessorViewer(metaclass=abc.ABCMeta):
    """Interfaz para los objetos en la composicion

    Implementa el comportamiento por defecto para la interfaz comun a todas las clases
    Declara la interfaz para acceder y gestionar sus hijos componentes
    Define la interfaz para acceder al componente padre en la estructura recursiva y la
    implementa si es necesario.
    """

    @abc.abstractmethod
    def display(self, repository):
        pass


class Plotter(ProcessorViewer):
    """Define el comportamiento para los componentes que tienen hijos

    Almacena hijos componentes
    Implementa operaciones relacionadas con hijos en la interfaz componente
    """

    def __init__(self):
        self._children = set()

    def display(self, repository):
        """
            Desplega el los hijos componenetes en repositorio  
        """
        for child in self._children:
            child.display(repository)

    def add(self, component):
        """
            Agrega un componenete
        """
        self._children.add(component)

    def remove(self, component):
        """
            Elimina un componenete
        """    
        self._children.discard(component)


class max(ProcessorViewer):
    """Representa una hoja en la composicion, no tiene hijos"""

    def display(self, repository):
        """Muestra grafico barras con temperaturas maximas por nodo

        Agrupa las mediciones por identificador y toma la temperatura
        maxima de cada grupo, luego las representa en un grafico,
        el mismo se guarda como maxs.png en carpeta images

        :param repository: El conjunto de datos completo, como data frame de pandas
        :type Repository:
        """

        ad = repository.groupby(by="Id",as_index=False)["temp"].max()
        eje_x = ad['Id']
        eje_y = ad['temp']
        plt.bar(eje_x, eje_y)
        plt.ylabel('Temperatura')
        plt.xlabel('ID sensores')
        plt.title('Temperaturas maximas por sensor')
        plt.savefig(f'../images/maxs.png')
        plt.show()


class min(ProcessorViewer):
    """Representa una hoja en la composicion, no tiene hijos."""

    def display(self, repository):
        """Muestra grafico barras con temperaturas minimas por nodo

        Agrupa las mediciones por identificador y toma la temperatura
        minima de cada grupo, luego las representa en un grafico,
        el mismo se guarda como mins.png en carpeta images

        :param repository: El conjunto de datos completo, como data frame de pandas
        :type Repository:
        """

        ad = repository.groupby(by="Id",as_index=False)["temp"].min()
        eje_x = ad['Id']
        eje_y = ad['temp']
        plt.bar(eje_x, eje_y)
        plt.ylabel('Temperatura')
        plt.xlabel('ID sensores')
        plt.title('Temperaturas minimas por sensor')
        plt.savefig(f'../images/mins.png')
        plt.show()


class mean(ProcessorViewer):
    """Representa una hoja en la composicion, no tiene hijos."""

    def display(self, repository):
        """Muestra grafico barras con temperaturas medias por nodo

        Agrupa las mediciones por identificador y toma la temperatura
        media de cada grupo, luego las representa en un grafico,
        el mismo se guarda como means.png en carpeta images

        :param repository: El conjunto de datos completo, como data frame de pandas
        :type Repository:
        """

        ad = repository.groupby(by="Id",as_index=False)["temp"].mean()
        eje_x = ad['Id']
        eje_y = ad['temp']
        plt.bar(eje_x, eje_y)
        plt.ylabel('Temperatura')
        plt.xlabel('ID sensores')
        plt.title('Temperaturas medias por sensor')
        plt.savefig(f'../images/means.png')
        plt.show()


class temp_vs_time(ProcessorViewer):
    """Representa una hoja en la composicion, no tiene hijos."""

    def display(self, repository):
        """Muestra graficos temperatura vs tiempo para cada nodo

        Agrupa las mediciones por identificador, ordenadas en el orden en que
        llegaron, y grafica la temperatura vs etiqueta de tiempo,
        cada figura se guarda en carpeta images con formato node_<Id>.png

        :param repository: El conjunto de datos completo, como data frame de pandas
        :type Repository:
        """

        xticks_num = 3
        grouped_ids = repository.groupby(by = 'Id', as_index=False)

        for key, item in grouped_ids:
            y = item['temp'].values
            x = item['timestamp'].values
            current_id = item['Id'].iloc[0]
            figure = plt.figure()
            plt.plot(x, y)
            plt.xlabel('Tiempo')
            plt.ylabel('Temperatura [°C]')
            plt.grid(True)
            plt.xticks(rotation = 45)
            lgnd = current_id
            if current_id == 13:
                lgnd = "Martin"
            elif current_id == 10:
                lgnd = "Jeremias"
            elif current_id == 6:
                lgnd = "Jesus"
            elif lgnd == 2:
                lgnd = "Nicolas"
            plt.legend([lgnd])
            axis = plt.gca()
            axis.xaxis.set_major_locator(plt.MaxNLocator(xticks_num))
            figure.tight_layout()
            plt.savefig(f'../images/node_{lgnd}.png')

        plt.show()

