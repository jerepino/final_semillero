
https://gitlab.com/jerepino/final_semillero/-/wikis/Proyecto-Integrador


## Conda environment

```bash
conda env create -f envs/environment.yml
conda activate finenv
pip install myst-parser sphinxcontrib-plantum
```
